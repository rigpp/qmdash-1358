﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Score : MonoBehaviour {

	// Use this for initialization
	void Start () {
		//スコア表示用のゲームオブジェクトを取得
		Text scoreLabel = GameObject.Find("Canvas/Score").GetComponent<Text>();
		scoreLabel.color = Color.red;
		//グローバルに宣言したスコアをResultMgrのスクリプトから読み込む
		int Score = ResultMgr.GetScoreData ();
		scoreLabel.text = Score.ToString() + " 点" ;
	}
}