﻿using UnityEngine;
using System.Collections;
using Novel;

public class GameStart : MonoBehaviour {	

	public void  NextScene(){
		//今いるシーンがTitleという名前であれば、Quizという名前のシーンに移動する
		if (Application.loadedLevelName == "Title") {
			NovelSingleton.StatusManager.callJoker("wide/demo","");
		}
	}
		
	public static int qCount;

	public void NextQuiz(){

		if (Application.loadedLevelName == "Result") {

			if(qCount < 2){
				qCount++;
				Application.LoadLevel ("Quiz");
			}else{
				qCount = 0;
				Application.LoadLevel ("Score");
			}
		}
	}

	public void  BackToTitle(){

		if (Application.loadedLevelName == "Score") {
			ResultMgr.SetScoreData(0);
			Application.LoadLevel ("Title");
		}
	}
}